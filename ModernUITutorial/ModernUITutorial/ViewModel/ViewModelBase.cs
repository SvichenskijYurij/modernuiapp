﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernUITutorial.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        public bool HasErrors
        {
            get
            {
                return _errorDictionary.Count > 0;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private Dictionary<string, List<string>> _errorDictionary = new Dictionary<string, List<string>>();

        public IEnumerable GetErrors(string propertyName)
        {
            return _errorDictionary[propertyName];
        }

        public void ValidateProperty(string propertyName)
        {
            if (_errorDictionary.ContainsKey(propertyName))
                _errorDictionary.Remove(propertyName);

            var context = new ValidationContext(this, null, null) { MemberName = propertyName };
            var validationCollection = new List<ValidationResult>();

            Validator.TryValidateProperty(this.GetType().GetProperty(propertyName).GetValue(this), context,
                validationCollection);

            var validationStrings = new List<string>();

            foreach (var item in validationCollection)
            {
                validationStrings.Add(item.ToString());
            }

            if (validationStrings.Count > 0)
            {
                _errorDictionary.Add(propertyName, validationStrings);

                if (ErrorsChanged != null)
                    ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void SetError(string propertyName, string error)
        {
            if (!_errorDictionary.ContainsKey(propertyName))
            {
                _errorDictionary.Add(propertyName, new List<string>() {error});
            }
            else
            {
                _errorDictionary[propertyName].Add(error);
            }
            ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }
    }
}
