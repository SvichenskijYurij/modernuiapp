﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Collections;

namespace ModernUITutorial.ViewModel
{
    public class DisplayViewModel : ViewModelBase
    {
        private string _inputNumber;

        public string InputNumber
        {
            get
            {
                return _inputNumber;
            }
            set
            {
                if(!HasErrors)
                _inputNumber = value;
                ValidateProperty("InputNumber");
                OnPropertyChanged("InputNumber");
            }
        }

        public DisplayViewModel()
        {
            _inputNumber = string.Empty;
        }
    }
}
