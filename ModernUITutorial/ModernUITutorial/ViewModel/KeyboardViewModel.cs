﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using CalculatorLogic;

namespace ModernUITutorial.ViewModel
{
    public class KeyboardViewModel : ViewModelBase
    {
        public ICommand NumberPressedCommand { get; set; }
        public ICommand ActionPressedCommand { get; set; }

        private readonly RPN _calculator;

        public KeyboardViewModel()
        {
            NumberPressedCommand = new RelayCommand(NumberPressed);
            ActionPressedCommand = new RelayCommand(ActionPressed);
            _calculator = new RPN();
        }

        public void NumberPressed(object parameter)
        {
            App.ModelWorker.DisplayModel.InputNumber += parameter.ToString();
        }

        public void ActionPressed(object parameter)
        {
            switch (parameter.ToString())
            {
                case "←":
                    if (!string.IsNullOrEmpty(App.ModelWorker.DisplayModel.InputNumber))
                        App.ModelWorker.DisplayModel.InputNumber = App.ModelWorker.DisplayModel.InputNumber.Remove(
                            App.ModelWorker.DisplayModel.InputNumber.Length - 1);
                    break;
                case "C":
                    App.ModelWorker.DisplayModel.InputNumber = string.Empty;
                    break;
                case "=":
                    try
                    {
                        var result = _calculator.Calculate(App.ModelWorker.DisplayModel.InputNumber);
                        App.ModelWorker.DisplayModel.InputNumber += " = " + result.ToString();
                    }
                    catch (Exception)
                    {
                        App.ModelWorker.DisplayModel.SetError("InputNumber", "Wrong expression string!");
                    }
                    break;
            }
        }
    }
}
