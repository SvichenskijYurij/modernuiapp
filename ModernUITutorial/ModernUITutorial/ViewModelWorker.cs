﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModernUITutorial.ViewModel;

namespace ModernUITutorial
{
    public class ViewModelWorker
    {
        private static ViewModelWorker _instance;

        public DisplayViewModel DisplayModel { get; private set; }
        public KeyboardViewModel KeyboardModel { get; private set; }

        private ViewModelWorker()
        {
            DisplayModel = new DisplayViewModel();
            KeyboardModel = new KeyboardViewModel();
        }

        public static ViewModelWorker GetInstance()
        {
            return _instance ?? (_instance = new ViewModelWorker());
        }
    }
}
