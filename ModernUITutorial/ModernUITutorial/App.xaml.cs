﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ModernUITutorial.ViewModel;

namespace ModernUITutorial
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ViewModelWorker ModelWorker { get { return ViewModelWorker.GetInstance(); } }
    }
}
